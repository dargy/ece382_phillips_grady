// Lab17_Control.c
// Runs on MSP432
// Implementation of the control system.
// Daniel and Jonathan Valvano
// July 11, 2019

/* This example accompanies the book
   "Embedded Systems: Introduction to Robotics,
   Jonathan W. Valvano, ISBN: 9781074544300, copyright (c) 2019
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2019, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "msp.h"
#include "../inc/Clock.h"
#include "../inc/CortexM.h"
#include "../inc/PWM.h"
#include "../inc/LaunchPad.h"
#include "../inc/UART0.h"
#include "../inc/Motor.h"
#include "../inc/Bump.h"
#include "../inc/ADC14.h"
#include "../inc/TimerA1.h"
#include "../inc/IRDistance.h"
#include "../inc/Nokia5110.h"
#include "../inc/LPF.h"
#include "../inc/SysTickInts.h"
#include "../inc/Tachometer.h"
#include "../inc/Reflectance.h"

#define TACHBUFF 1

int32_t ActualSpeedL, ActualSpeedR;   	 // Actual speed
int32_t ErrorL, ErrorR;     			 // X* - X'
int32_t PrevErrorL, PrevErrorR;
int32_t Mode;
int32_t UL, UR;             // Controller output PWM duty 2 to 14,998
uint32_t Time; // in 0.01 sec
uint16_t LeftTach[TACHBUFF];             // tachometer period of left wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection LeftDir;              // direction of left rotation (FORWARD, STOPPED, REVERSE)
int32_t LeftSteps;                       // number of tachometer steps of left wheel (units of 220/360 = 0.61 mm traveled)
uint16_t RightTach[TACHBUFF];            // tachometer period of right wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection RightDir;             // direction of right rotation (FORWARD, STOPPED, REVERSE)
int32_t RightSteps;                      // number of tachometer steps of right wheel (units of 220/360 = 0.61 mm traveled)

#define PWMNOMINAL 6000

/**************Functions used by all programs***********************/
void Pause3(void){
    int j;
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(20); LaunchPad_Output(0); // off
    Clock_Delay1ms(20); LaunchPad_Output(1); // red
  }
  while(Bump_Read()==0){// wait for touch
    Clock_Delay1ms(10); LaunchPad_Output(0); // off
    Clock_Delay1ms(10); LaunchPad_Output(3); // red/green
  }
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(10); LaunchPad_Output(0); // off
    Clock_Delay1ms(10); LaunchPad_Output(4); // blue
  }
  for(j=600;j>100;j=j-200){
    Clock_Delay1ms(j/10); LaunchPad_Output(0); // off
    Clock_Delay1ms(j/10); LaunchPad_Output(2); // green
  }
  // restart Jacki
  Mode = 1;
  UR = UL = PWMNOMINAL;    // reset parameters
  Time = 0;
}

/**************Program17_3******************************************/
#define DESIRED_SPEEDL 86
#define DESIRED_SPEEDR 87
#define SWING3 4000
#define PWMIN (PWMNOMINAL-SWING3)
#define PWMAX (PWMNOMINAL+SWING3)
#define BACKUPTIME 140
#define STEPSFOR90DEGLEFT 92
#define STEPSFOR90DEGRIGHT 123
#define RIGHTTURN 100
#define LEFTTURN 200
#define SIDECUTOFF 0x4

enum FSMState {
	Stop,
	Go,
	Back,
	Turn,
	LineStop
};

enum FSMState state;
enum scenario_t maze;
bool has_hit_wall;
bool has_turned_around;
bool has_backed_up;
int32_t i;
uint32_t time_started_backup;
int32_t right_steps_on_hit;
int32_t right_steps_delta_goal;
int32_t num_turns = 0;
int32_t numBitSections = 0;
bool passed_ignored_turn = false;
int32_t ignoredTurnAtTime = 0;
int32_t nextTurnDirection;


// Proportional controller to drive robot using line following
uint8_t LineData= 0;       // direct measure from line sensor
int32_t Position;      // position in 0.1mm relative to center of line

// proportional controller gain
// experimentally determine value that creates a stable system
int32_t Kp3 = 15;
int32_t stopTimer = 0;
int32_t Ka = 3;
int32_t Kb = 0;

uint32_t avg(uint16_t *array, int length){
  int i;
  uint32_t sum = 0;
  for(i = 0; i < length; i = i + 1) {
    sum = sum + array[i];
  }

  return (sum / length) != 0 ? (sum / length) : 1;
}

void turn_setup(int32_t turnDirection) {
	Tachometer_Get(LeftTach, &LeftDir, &LeftSteps, RightTach, &RightDir, &RightSteps);
	right_steps_on_hit = RightSteps;
	right_steps_delta_goal = (turnDirection == LEFTTURN) ? STEPSFOR90DEGLEFT : STEPSFOR90DEGRIGHT;
}

void stop() {
	Motor_Stop();

	if (stopTimer < 200)
		LaunchPad_Output(1);
	else if (stopTimer < 400)
		LaunchPad_Output(4);
	else
		stopTimer = 0;

	stopTimer++;
}

void go() {
    if(Mode) {
		// pull tachometer information
		i = (i+1)%TACHBUFF;
        Tachometer_Get(LeftTach + i, &LeftDir, &LeftSteps, RightTach + i, &RightDir, &RightSteps);

		// use average of ten tachometer values to determine
		// actual speed (similar to Lab16)
		ActualSpeedL = 2000000 / avg(LeftTach, TACHBUFF);
		ActualSpeedR = 2000000 / avg(RightTach, TACHBUFF);

		// use proportional control to update duty cycle
		// LeftDuty = LeftDuty + Kp*LeftError
		ErrorR = DESIRED_SPEEDR - ActualSpeedR;
		ErrorL = DESIRED_SPEEDL - ActualSpeedL;

		UR = UR + Ka * ErrorR + Kb * PrevErrorR;
		UL = UL + Ka * ErrorL + Kb * PrevErrorL;

		// check min/max duty values
		if (UR > PWMAX1) UR = PWMAX1;
		if (UL > PWMAX1) UL = PWMAX1;
		if (UR < PWMIN1) UR = PWMIN1;
		if (UL < PWMIN1) UL = PWMIN1;

		PrevErrorL = ErrorL;
		PrevErrorR = ErrorR;

		// update motor values
        Motor_Forward(UL, UR);
		ControllerFlag = 1;
    }
}

void back() {
	if (time_started_backup + BACKUPTIME < Time) {
		turn_setup(nextTurnDirection);
		state = Turn;
	}

	Motor_Backward(PWMNOMINAL, PWMNOMINAL);
}

void turn() {
	if (nextTurnDirection == LEFTTURN) {
		Motor_Left(PWMNOMINAL, PWMNOMINAL);
	} else {
		Motor_Right(PWMNOMINAL, PWMNOMINAL);
	}

	Tachometer_Get(LeftTach, &LeftDir, &LeftSteps, RightTach, &RightDir, &RightSteps);
	if (abs(RightSteps - right_steps_on_hit) >= right_steps_delta_goal) {
		state = Go;
		num_turns++;
	}
}

void IRsampling(void){
    uint32_t raw17, raw12, raw16;
    ADC_In17_14_16(&raw17, &raw12, &raw16);
    nr = LPF_Calc(raw17);
    nc = LPF_Calc2(raw12);
    nl = LPF_Calc3(raw16);
    Left = LeftConvert(nl);
    Center = CenterConvert(nc);
    Right = RightConvert(nr);
    ADCflag = 1;
}

/*
* Proportional controller to drive robot
* using line following
*/
int32_t change = 0;
void Controller(void) {
	// read values from line sensor, similar to
	// SysTick_Handler() in Lab10_Debugmain.c
	switch(state) {
		case Go:
			go();
			break;
		case Stop:
			stop();
			break;
		case Turn:
			turn();
			break;
		case Back:
			back();
			break;
	}


	Time++;
}

// proportional control, line following
void main(void) {
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Reflectance_Init();
    Motor_Init();
    Tachometer_Init();

	// user TimerA1 to run the controller at 1000 Hz
	// replace this line with a call to TimerA1_Init()
	TimerA1_Init(&Controller, 500);

    Motor_Stop();
    state = Go;
    Mode = 0;
    Time = 0;
    UR = UL = PWMNOMINAL;
    EnableInterrupts();
    Pause3();
    while(1) {
		if (Bump_Read() != 0) {
			if (state == Go) {
				time_started_backup = Time;
				nextTurnDirection = Bump_Read() > SIDECUTOFF ? RIGHTTURN : LEFTTURN;
				state = Back;
			} else {
//				Pause3();
			}
		}
    }
}
