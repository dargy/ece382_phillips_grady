; Encryption.asm
; Runs on any Cortex M
; Student name:
; Date:
; Basic XOR encryption and decryption functions for cryptography.
; Capt Steven Beyer
; July 23 2020
;
;Simplified BSD License (FreeBSD License)
;Copyright (c) 2020, Steven Beyer, All rights reserved.
;
;Redistribution and use in source and binary forms, with or without modification,
;are permitted provided that the following conditions are met:
;
;1. Redistributions of source code must retain the above copyright notice,
;   this list of conditions and the following disclaimer.
;2. Redistributions in binary form must reproduce the above copyright notice,
;   this list of conditions and the following disclaimer in the documentation
;   and/or other materials provided with the distribution.
;
;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
;LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
;AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
;OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
;USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;
;The views and conclusions contained in the software and documentation are
;those of the authors and should not be interpreted as representing official
;policies, either expressed or implied, of the FreeBSD Project.


       .thumb
       .text
       .align 2
       .global Encrypt
       .global Decrypt
       .global XOR_bytes

;------------Encrypt------------
; Takes in the location of a plain text message and key and
;	xors the message to encrypt it. It stores the
;	result at a passed in memory location. Stops
;	when the ASCII character # is encountered.
; Input:
;	R0 is message address,
;	R1 is key address,
;	R2 is location to store encrypted message
; Output: encrypted message stored at memory
;	located ate R2
; Modifies: R0, R1
Encrypt:   .asmfunc
	; save registers
	PUSH {R3-R12}

	; select first byte of key and clear the rest of the bytes
	AND R1, R1, #0xFF
	MOV	R11, R2  ; msg address to R11
	MOV R10, R0
	MOV R9, R1

	; loop
loope
	; retrieve next byte of message
	LDRB R4, [R10], #1 ;

	; XOR_Bytes
	MOV R0, R4
	MOV R1, R9
	PUSH {LR}
	BL XOR_bytes
	POP {LR}
	STRB R0, [R11], #1

	; Compare byte from message with end of message
	CMP R4, #0x23

	; loop or end
	BEQ donee
	B loope
donee
	; restore messages
	POP {R3-R12}
	BX LR
		.endasmfunc

;------------Decrypt------------
; Takes in the location of an encrypted message and key and
;	xors the message to decrypt it it. It stores the
;	result at a passed in memory location. Stops
;	when the ASCII character # is encountered.
; Input: R0 is message address, R1 is key address,
;	R2 is location to store decrypted message
; Output: decrypted message stored at memory
;	located ate R2
; Modifies: R0, R1
Decrypt:	.asmfunc
	; save registers
	PUSH {R3-R11}
	; select first byte of key and clear the rest of the bytes
	AND R1, R1, #0xFF
	MOV R11, R2 ; final msg address
	MOV R10, R0 ; unencrypted add
	MOV R9, R1	; key address

	; loop
loopd

	; retrieve next byte of message
	LDRB R4, [R10], #1

	; XOR_Bytes
	MOV R0, R4
	MOV R1, R9
	PUSH {LR}
	BL XOR_bytes
	POP {LR}
	STRB R0, [R11], #1

	; Compare byte from decrypted with end of message
	CMP R0, #0x23

	; loop or end
	BEQ doned
	B loopd

doned
	; restore messages
	POP {R3-R11}
	BX LR
		.endasmfunc

;------------XOR_Bytes------------
; Takes in two bytes, XORs them, returns the result
; Input: R0 message byte, R1 is key byte,
; Output: R0 is XOR result
; Modifies: R0
XOR_bytes:	.asmfunc
	; eor
	EOR R0, R0, R1
	BX LR
		.endasmfunc
		.align 4
	;End of Message character, #
    .end
