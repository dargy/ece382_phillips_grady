; Cryptography.asm
; Runs on any Cortex M
; Student name: Grady Phillips
; Date: Aug 21 2020
; Documentation statement: None
; Cryptography function to encrypt and decrypt a message using a 1 byte key.
; Capt Steven Beyer
; July 23 2020
;
;Simplified BSD License (FreeBSD License)
;Copyright (c) 2020, Steven Beyer, All rights reserved.
;
;Redistribution and use in source and binary forms, with or without modification,
;are permitted provided that the following conditions are met:
;
;1. Redistributions of source code must retain the above copyright notice,
;   this list of conditions and the following disclaimer.
;2. Redistributions in binary form must reproduce the above copyright notice,
;   this list of conditions and the following disclaimer in the documentation
;   and/or other materials provided with the distribution.
;
;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
;LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
;AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
;OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
;USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;
;The views and conclusions contained in the software and documentation are
;those of the authors and should not be interpreted as representing official
;policies, either expressed or implied, of the FreeBSD Project.
       .thumb
       .data
       .align 2
		; variables
encrypted_message .space 32
decrypted_message .space 32
       .text
       .align 2
		;variables
emessage	.word 0x71070107, 0x2B62707A
			.word 0x2A366231, 0x27206227
			.word 0x21623631, 0x3131232E
			.word 0x00006163
message1 	.string "This is message1!#"
message2 	.string "This is message2!#"
message3 	.string "This is message3!#"
key1		.byte 	0xab
key2		.byte 	0xcd
key3		.byte 	0xef
ekey		.byte	0x42
       .global Encrypt
       .global Decrypt
       .global main

main:  .asmfunc
	 ; load registers
	 LDR R0, emsgptr
	 LDR R1, key1
	 LDR R2, encptr

     ; encrypt the message
     BL Encrypt
     NOP


     ; load registers
	 LDR R0, emsgptr
	 LDR R1, ekey
	 LDR R2, decptr

     ; decrypt the message
	 BL Decrypt
quit
	 B quit

    .endasmfunc
    ; pointers to variables
encptr	.word	encrypted_message
decptr	.word	decrypted_message
msg1ptr .word	message1
msg2ptr	.word	message2
msg3ptr	.word	message3
key1ptr	.word	key1
key2ptr	.word	key2
key3ptr	.word	key3
ekeyptr	.word	ekey
emsgptr	.word	emessage
    .end
