; LEDs.asm
; Runs on MSP432
; Capt Steven Beyer
; September 9, 2019


;	Code to activate LED on P6.7. This code accompanies the Lab08_LED_Switchesmain.c
;
       .thumb
       .text
       .align 2
       .global LED_Init
       .global LED_Off
       .global LED_On
       .global LED_Toggle
       .global LED_Oscillate

; function to initialize P6.7
LED_Init:	.asmfunc
	LDR R1, P6SEL0
	LDRB R0, [R1]
	BIC R0, R0, #0x80	; GPIO
	STRB R0, [R1]
	LDR R1, P6SEL1
	LDRB R0, [R1]
	BIC R0, R0, #0x80
	STRB R0, [R1]		; GPIO
	LDR R1, P6DIR
	LDRB R0, [R1]
	ORR R0, R0, #0x80	; output
	STRB R0, [R1]
	BX LR
	.endasmfunc

; function to turn off P6.7
LED_Off:		.asmfunc
	LDR R1, P6OUT
	LDRB R0, [R1]		; 8-bit read
	BIC R0, R0, #0x80	; turn off
	STRB R0, [R1]
	BX LR
	.endasmfunc

; function to turn on P6.7
LED_On:	.asmfunc
	LDR R1, P6OUT
	LDRB R0, [R1]		; 8-bit read
	ORR R0, R0, #0x80	; turn on
	STRB R0, [R1]
	BX LR
	.endasmfunc

; function to toggle P6.7
LED_Toggle: .asmfunc
	LDR R1, P6OUT 		; load &P6OUT into R1
	LDRB R0, [R1] 		; load P6OUT into R0
	EOR R0, R0, #0x80 	; toggle bit
	STRB R0, [R1] 		; store bit back into P6OUT
	BX LR 				; return
	.endasmfunc

; function to continuously toggle P6.7 every half second
; use a loop as a timer
LED_Oscillate:	.asmfunc
	LDR R2, DELAY ; load delay into R2
	MOV R0, #0    ; set R0 to 0

loop:       		; label for looping
	CMP R0, R2    	; see if R0 = R2
	BEQ equal     	; go to equal if R0 = R2
	ADD R0, R0, #1 	; increment R0
	B loop        	; branch to loop hehe xd 
equal:           	; label to get branched to if R0 = R2
	BL LED_Toggle 	; led toggle
	MOV R0, #0    	; reset R0
	B loop    		; branch to loop hehe xd
	.endasmfunc
; addresses for Port 6 registers
	.align 4
P6SEL0 .word 0x40004C4B ; address from datasheet
P6SEL1 .word 0x40004C4D ; address from datasheet
P6DIR  .word 0x40004C45 ; address from datasheet
P6OUT  .word 0x40004C43 ; address from datasheet
DELAY  .word 2000000    ; from testing with logic analyzer
	.end
