/*
 * Classifier.h
 * Runs on MSP432
 *
 * Student name: Grady Phillips
 * Date:
 *
 * Conversion function for a GP2Y0A21YK0F IR sensor and classification function
 *  to take 3 distance readings and determine what the current state of the robot is.
 *
 *  Created on: Jul 24, 2020
 *  Author: Captain Steven Beyer
 *
 */

enum scenario {
    Error = 0,
	LeftTooClose = 1,
	RightTooClose = 2,
	CenterTooClose = 4,
	Straight = 8,
	LeftTurn = 9,
	RightTurn = 10,
	TeeJoint = 11,
	LeftJoint = 12,
	RightJoint = 13,
	CrossRoad = 14,
	Blocked = 15
};

typedef enum scenario scenario_t;

scenario_t Classify(int32_t Left, int32_t Center, int32_t Right);

int32_t Convert(int32_t n);
