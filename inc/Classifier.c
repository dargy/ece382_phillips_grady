/*
 * Classifier.c
 * Runs on MSP432
 *
 * Student name:
 * Date:
 *
 * Conversion function for a GP2Y0A21YK0F IR sensor and classification function
 *  to take 3 distance readings and determine what the current state of the robot is.
 *
 *  Created on: Jul 24, 2020
 *  Author: Captain Steven Beyer
 *
 * This example accompanies the book
 * "Embedded Systems: Introduction to Robotics,
 * Jonathan W. Valvano, ISBN: 9781074544300, copyright (c) 2019
 * For more information about my classes, my research, and my books, see
 * http://users.ece.utexas.edu/~valvano/
 *
 * Simplified BSD License (FreeBSD License
 *
 * Copyright (c) 2019, Jonathan Valvano, All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the FreeBSD Project.
 */

#include <stdint.h>
#include "../inc/Classifier.h"

#define IRSlope	1195172 // used for convert
#define IROffset -1058 // used for convert
#define IRMax 2552 // used for convert
#define MaxDist 800 // maxdist for adc value
#define SIDEMAX 354 // largest side distance to wall in mm
#define SIDEMIN 140 // smallest side distance to wall in mm
#define CENTEROPEN 500// distance to wall between open/blocked
#define CENTERMIN 90 // min distance to wall in the front
#define IRMIN 50	// min possible reading of IR sensor
#define IRMAX 1000	// max possible reading of IR sensor

/* Convert
* Calculate the distance in mm given the 14-bit ADC value
* D = 1195172/(n - 1058)
*
* The maximum measurement distance for the sensor is 800 mm,
* so if the ADC value is less than 2552 (IRMAX), your
* function should return 800.
*
* Input
*   int32_t n:  14-bit ADC data
* Output
*   int32_t     Distance in mm
*/
int32_t Convert(int32_t n){
	if (n < IRMax) {
		return MaxDist;
	}

    return IRSlope/(n + IROffset);
}

/* Classify
* Utilize three distance values from the left, center, and right
* distance sensors to determine and classify the situation into one
* of many scenarios (enumerated by scenario)
*
* Input
*   int32_t Left: distance measured by left sensor
*   int32_t Center: distance measured by center sensor
*   int32_t Right: distance measured by right sensor
*
* Output
*   scenario_t: value representing the scenario the robot
*       currently detects (e.g. RightTurn, LeftTurn, etc.)
*/
scenario_t Classify(int32_t Left, int32_t Center, int32_t Right){
    scenario_t result = Error;

    // return error if any < 50
    if (Left < IRMIN || Center < IRMIN || Right < IRMIN) {
    	return result;
    }

    // return error if any > 800
    if (Left > IRMAX || Center > IRMAX || Right > IRMAX) {
    	return result;
    }

    if (Left < SIDEMIN || Right < SIDEMIN || Center < CENTERMIN) {
        // check LeftTooClose conditions, multiple conditions
        if (Left < SIDEMIN) {
            result = LeftTooClose;
        }

        // check if CenterTooClose conditions
        if (Center < CENTERMIN) {
            result = CenterTooClose;
        }

        // check RightTooClose conditions
        if (Right < SIDEMIN) {
            result = RightTooClose;
        }

        // check if left and right too close, return sum
        if (Left < SIDEMIN && Right < SIDEMIN) {
            result = LeftTooClose + RightTooClose; // 3
        }

        // check if left and center too close, return sum
        if (Left < SIDEMIN && Center < CENTERMIN) {
            result = LeftTooClose + CenterTooClose; // 5
        }

        // check if center and right too close, return sum
        if (Center < CENTERMIN && Right < SIDEMIN) {
            result = CenterTooClose + RightTooClose; // 5
        }

        // check if all three directions too close, return sum
        if (Left < SIDEMIN && Center < CENTERMIN && Right < SIDEMIN) {
            result = LeftTooClose + CenterTooClose + RightTooClose; // 5
        }
        // check LeftTooClose conditions, multiple conditions
        if (Left < SIDEMIN) {
            result = LeftTooClose;
        }

        // check if CenterTooClose conditions
        if (Center < CENTERMIN) {
            result = CenterTooClose;
        }

        // check RightTooClose conditions
        if (Right < SIDEMIN) {
            result = RightTooClose;
        }

        // check if left and right too close, return sum
        if (Left < SIDEMIN && Right < SIDEMIN) {
            result = LeftTooClose + RightTooClose; // 3
        }

        // check if left and center too close, return sum
        if (Left < SIDEMIN && Center < CENTERMIN) {
            result = LeftTooClose + CenterTooClose; // 5
        }

        // check if center and right too close, return sum
        if (Center < CENTERMIN && Right < SIDEMIN) {
            result = CenterTooClose + RightTooClose; // 5
        }

        // check if all three directions too close, return sum
        if (Left < SIDEMIN && Center < CENTERMIN && Right < SIDEMIN) {
            result = LeftTooClose + CenterTooClose + RightTooClose; // 5
        }
    } else {
        // Center < CENTEROPEN condition to check if
        // the center path is closed
        if (Center < CENTEROPEN) {
            // check if right open
            if (Right >= SIDEMAX) {
                result = RightTurn;
            }

            // check if left open
            if (Left >= SIDEMAX) {
                result = LeftTurn;
            }

            // check if both open
            if (Left >= SIDEMAX && Right >= SIDEMAX) {
                result = TeeJoint;
            }

            // check if both closed
            if (Left < SIDEMAX && Right < SIDEMAX) {
                result = Blocked;
            }
        } else {
            // check if right open
            if (Right >= SIDEMAX) {
                result = RightJoint;
            }

            // check if left open
            if (Left >= SIDEMAX) {
                result = LeftJoint;
            }

            // check if both open
            if (Left >= SIDEMAX && Right >= SIDEMAX) {
                result = CrossRoad;
            }

            // check if both closed
            if (Left < SIDEMAX && Right < SIDEMAX) {
                result = Straight;
            }
        }
    }

    return result;
}

