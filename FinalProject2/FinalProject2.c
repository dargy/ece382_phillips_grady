// Lab17_Control.c
// Runs on MSP432
// Implementation of the control system.
// Daniel and Jonathan Valvano
// July 11, 2019

/* This example accompanies the book
   "Embedded Systems: Introducion to Robotics,
   Jonathan W. Valvano, ISBN: 9781074544300, copyright (c) 2019
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2019, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "msp.h"
#include "../inc/Clock.h"
#include "../inc/CortexM.h"
#include "../inc/PWM.h"
#include "../inc/LaunchPad.h"
#include "../inc/UART0.h"
#include "../inc/Motor.h"
#include "../inc/Bump.h"
#include "../inc/ADC14.h"
#include "../inc/TimerA1.h"
#include "../inc/IRDistance.h"
#include "../inc/Nokia5110.h"
#include "../inc/LPF.h"
#include "../inc/SysTickInts.h"
#include "../inc/Tachometer.h"
#include "../inc/Reflectance.h"

#define TACHBUFF 1
int32_t Mode;
int32_t UL, UR;             // Controller output PWM duty 2 to 14,998
uint32_t Time; // in 0.01 sec
uint16_t LeftTach[TACHBUFF];             // tachometer period of left wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection LeftDir;              // direction of left rotation (FORWARD, STOPPED, REVERSE)
int32_t LeftSteps;                       // number of tachometer steps of left wheel (units of 220/360 = 0.61 mm traveled)
uint16_t RightTach[TACHBUFF];            // tachometer period of right wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection RightDir;             // direction of right rotation (FORWARD, STOPPED, REVERSE)
int32_t RightSteps;                      // number of tachometer steps of right wheel (units of 220/360 = 0.61 mm traveled)

#define PWMNOMINAL 3500

/**************Functions used by all programs***********************/
void Pause3(void){
    int j;
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(20); LaunchPad_Output(0); // off
    Clock_Delay1ms(20); LaunchPad_Output(1); // red
  }
  while(Bump_Read()==0){// wait for touch
    Clock_Delay1ms(10); LaunchPad_Output(0); // off
    Clock_Delay1ms(10); LaunchPad_Output(3); // red/green
  }
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(10); LaunchPad_Output(0); // off
    Clock_Delay1ms(10); LaunchPad_Output(4); // blue
  }
  for(j=600;j>100;j=j-200){
    Clock_Delay1ms(j/10); LaunchPad_Output(0); // off
    Clock_Delay1ms(j/10); LaunchPad_Output(2); // green
  }
  // restart Jacki
  Mode = 1;
  UR = UL = PWMNOMINAL;    // reset parameters
  Time = 0;
}

/**************Program17_3******************************************/
#define SWING3 3000
#define PWMIN3 (PWMNOMINAL-SWING3)
#define PWMAX3 (PWMNOMINAL+SWING3)
#define BACKUPTIME 430
#define STEPSFOR90DEGLEFT 150
#define STEPSFOR90DEGRIGHT 185

enum FSMState {
	Stop,
	Go,
	Back,
	Turn,
	LineStop
};

enum FSMState state;
bool has_hit_wall;
bool has_turned_around;
bool has_backed_up;
uint32_t time_started_backup;
int32_t rights_steps_on_hit;
int32_t right_steps_delta_goal;
int32_t num_turns = 0;
int32_t numSetBits;
int32_t numBitSections;
bool passed_ignored_turn = false;
int32_t ignoredTurnAtTime = 0;

// Proportional controller to drive robot using line following
uint8_t LineData;       // direct measure from line sensor
int32_t Position;      // position in 0.1mm relative to center of line

// proportional controller gain
// experimentally determine value that creates a stable system
int32_t Kp3 = 15;
int32_t stopTimer = 0;

int countSetBitsLineData(uint8_t n) {
	int count = 0;
	while (n) {
		count += n & 1;
		n >>= 1;
	}
	return count;
}

int countBitSections(uint8_t n) {
	int count = 0;
	int startBit = n & 1;
	int lastBit = 0;
	do {
		if (startBit == 1 && lastBit == 0) {
			count++;
		}

		n >>= 1;
		lastBit = n & 1;
	} while(n);

	return count;
}

void stop() {
	Motor_Stop();

	if (stopTimer < 200)
		LaunchPad_Output(1);
	else if (stopTimer < 400)
		LaunchPad_Output(4);
	else
		stopTimer = 0;

	stopTimer++;
}

void go() {
    if (Time % 10 == 0) {
        Reflectance_Start();
    }

    // run reflectance end every subsequent call
    if (Time % 10 == 1) {
        LineData = Reflectance_End();
        	numSetBits = countSetBitsLineData(~LineData);
        	numBitSections = countBitSections(~LineData);
        	if (numBitSections > 2) {
        		state = Stop;
        	}

		// Use Reflectance_Position() to find position
		Position = Reflectance_Position(LineData);
		if (numSetBits > 3) {
			Position = 0;
		}

		if(Mode && state == Go) {
			// update duty cycle based on porportional control
			UR = PWMNOMINAL + Kp3 * Position;
			UL = PWMNOMINAL + -Kp3 * Position;

			// limit change to within swing
			if (UR > PWMAX3) UR = PWMAX3;
			if (UL > PWMAX3) UL = PWMAX3;
			if (UR < PWMIN3) UR = PWMIN3;
			if (UL < PWMIN3) UL = PWMIN3;

			Motor_Forward(UL, UR);
		}
    }
}

void back() {
	if (time_started_backup + BACKUPTIME < Time) {
		Tachometer_Get(LeftTach, &LeftDir, &LeftSteps, RightTach, &RightDir, &RightSteps);
		rights_steps_on_hit = RightSteps;
		right_steps_delta_goal = num_turns == 3 ? STEPSFOR90DEGRIGHT : STEPSFOR90DEGLEFT;
		state = Turn;
	}

	Motor_Backward(PWMNOMINAL, PWMNOMINAL);
}

void turn() {
	if (num_turns == 3) {
		Motor_Right(PWMNOMINAL, PWMNOMINAL);
	} else {
		Motor_Left(PWMNOMINAL, PWMNOMINAL);
	}

	Tachometer_Get(LeftTach, &LeftDir, &LeftSteps, RightTach, &RightDir, &RightSteps);
	if (abs(RightSteps - rights_steps_on_hit) >= right_steps_delta_goal) {
		state = Go;
		num_turns++;
	}
}

/*
* Proportional controller to drive robot
* using line following
*/
int32_t change = 0;
void Controller(void) {
	// read values from line sensor, similar to
	// SysTick_Handler() in Lab10_Debugmain.c
	switch(state) {
		case Go:
			go();
			break;
		case Stop:
			stop();
			break;
		case Turn:
			turn();
			break;
		case Back:
			back();
			break;
	}


	Time++;
}

// proportional control, line following
void main(void) {
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Reflectance_Init();
    Motor_Init();
    Tachometer_Init();

	// user TimerA1 to run the controller at 1000 Hz
	// replace this line with a call to TimerA1_Init()
	TimerA1_Init(&Controller, 500);

    Motor_Stop();
    state = Go;
    Mode = 0;
    Time = 0;
    UR = UL = PWMNOMINAL;
    EnableInterrupts();
    Pause3();
    while(1) {
		if (Bump_Read() != 0) {
			if (state == Go) {
				time_started_backup = Time;
				state = Back;
				if (num_turns == 4) {
					state = Stop;
				}
			} else {
//				Pause3();
			}
		}
    }
}
