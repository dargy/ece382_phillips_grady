// Lab15_ADCmain.c
// Runs on MSP432, RSLK1.1
// Test the operation of the GP2Y0A21YK0F infrared distance
// sensors by repeatedly taking measurements.  Either
// perform statistical analysis of the data, stream data
// directly from all three channels, or stream calibrated
// measurements from all three channels.  In this case, the
// results are sent through the UART to a computer running
// TExaSdisplay or another terminal program.
// Daniel Valvano
// July 11, 2019

/* This example accompanies the book
   "Embedded Systems: Introduction to Robotics,
   Jonathan W. Valvano, ISBN: 9781074544300, copyright (c) 2019
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2019, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

// 5V  connected to all three Pololu #136 GP2Y0A21YK0F Vcc's (+5V)
// ground connected to all three Pololu #136 GP2Y0A21YK0F grounds
// MSP432 P9.0 (J5) (analog input A17 to MSP432) connected to right GP2Y0A21YK0F Vout
// MSP432 P6.1 (J3.23) (analog input A14 to MSP432) connected to center GP2Y0A21YK0F Vout
// MSP432 P9.1 (J5) (analog input A16 to MSP432) connected to left GP2Y0A21YK0F Vout

#include <stdint.h>
#include "msp.h"
#include "../inc/Clock.h"
#include "../inc/CortexM.h"
#include "../inc/IRDistance.h"
#include "../inc/TimerA1.h"
#include "../inc/UART0.h"
#include "../inc/LaunchPad.h"
#include "../inc/ADC14.h"
#include "../inc/LPF.h"
#include "../inc/nokia5110.h"



//**************Program 15.2*******************
// Used to find callibration coefficients for convert
//	Tests void ADC0_InitSWTriggerCh17_14_16(void) and
//	void ADC_In17_14_16(uint32_t *ch17, uint32_t *ch14, uint32_t *ch16)
volatile uint32_t nr,nc,nl;
volatile uint32_t ADCflag;

void LCDClear1(void){
  Nokia5110_Init();
  Nokia5110_Clear(); // erase entire display
  Nokia5110_OutString("15: ADC");
  Nokia5110_SetCursor(0,1); Nokia5110_OutString("IR LPF value");
  Nokia5110_SetCursor(0,2); Nokia5110_OutString("L= ");
  Nokia5110_SetCursor(0,3); Nokia5110_OutString("C= ");
  Nokia5110_SetCursor(0,4); Nokia5110_OutString("R= ");
}

void LCDOut1(void){
  Nokia5110_SetCursor(3,2); Nokia5110_OutUDec(nl);
  Nokia5110_SetCursor(3,3); Nokia5110_OutUDec(nc);
  Nokia5110_SetCursor(3,4); Nokia5110_OutUDec(nr);
}
void Program15_1_ISR(void){  // runs at 2000 Hz
  uint32_t raw17,raw14,raw16;
  P1OUT ^= 0x01;         // profile
  P1OUT ^= 0x01;         // profile
  ADC_In17_14_16(&raw17,&raw14,&raw16);  // sample
  nr = LPF_Calc(raw17);  // right is channel 17 P9.0
  nc = LPF_Calc2(raw14); // center is channel 14, P4.1
  nl = LPF_Calc3(raw16); // left is channel 16, P9.1
  ADCflag = 1;           // semaphore
  P1OUT ^= 0x01;         // profile
}
// Calibration and testing
int Program15_1(void){ // example program 15.2
  uint32_t raw17,raw14,raw16;
  int32_t n; uint32_t s;
  Clock_Init48MHz();
  ADCflag = 0;
  s = 256; // replace with your choice
  ADC0_InitSWTriggerCh17_14_16();   // initialize channels 17,14,16
  ADC_In17_14_16(&raw17,&raw14,&raw16);  // sample
  LPF_Init(raw17,s);     // P9.0/channel 17
  LPF_Init2(raw14,s);    // P4.1/channel 14
  LPF_Init3(raw16,s);    // P9.1/channel 16
  UART0_Init();          // initialize UART0 115,200 baud rate
  LCDClear1();
  LaunchPad_Init();
  TimerA1_Init(&Program15_1_ISR,250);    // 2000 Hz sampling
  UART0_OutString("Program 15.2 Calibration test\nValvano July 2017\nConnect analog signals to P9.0,P4.1,P9.1\n");
  EnableInterrupts();
  while(1){
    for(n=0; n<2000; n++){
      while(ADCflag == 0){};
      ADCflag = 0; // show every 2000th point
    }
    LCDOut1();
//    // left
    UART0_OutUDec5(nl); UART0_OutUDec5(LeftConvert(nl));UART0_OutChar(' ');
//    // center
    UART0_OutUDec5(nc); UART0_OutUDec5(CenterConvert(nc));UART0_OutChar(' ');
//    // right
    UART0_OutUDec5(nr); UART0_OutUDec5(RightConvert(nr));
    UART0_OutChar('\n'); // once a second
  }
}

//**************Program 15.3*******************
// Used to test convert function
void LCDClear2(void){
  Nokia5110_Init();
  Nokia5110_Clear(); // erase entire display
  Nokia5110_OutString("15: ADC");
  Nokia5110_SetCursor(0,1); Nokia5110_OutString("IR distance");
  Nokia5110_SetCursor(0,2); Nokia5110_OutString("L= "); Nokia5110_OutUDec('0'); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,3); Nokia5110_OutString("C= "); Nokia5110_OutUDec('0'); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,4); Nokia5110_OutString("R= "); Nokia5110_OutUDec('0'); Nokia5110_OutString(" mm");
}

void LCDOut2(void){
  Nokia5110_SetCursor(3,2); Nokia5110_OutUDec(LeftConvert(nl));
  Nokia5110_SetCursor(3,3); Nokia5110_OutUDec(CenterConvert(nc));
  Nokia5110_SetCursor(3,4); Nokia5110_OutUDec(RightConvert(nr));
}

// Test
int Program15_2(void){
  uint32_t raw17,raw14,raw16;
  int32_t n; uint32_t s;
  Clock_Init48MHz();
  ADCflag = 0;
  s = 256; // replace with your choice
  ADC0_InitSWTriggerCh17_14_16();   // initialize channels 17,14,16
  ADC_In17_14_16(&raw17,&raw14,&raw16);  // sample
  LPF_Init(raw17,s);     // P9.0/channel 17
  LPF_Init2(raw14,s);    // P4.1/channel 14
  LPF_Init3(raw16,s);    // P9.1/channel 16
  UART0_Init();          // initialize UART0 115,200 baud rate
  LCDClear2();
  LaunchPad_Init();
  TimerA1_Init(&Program15_1_ISR,250);    // 2000 Hz sampling
  UART0_OutString("GP2Y0A21YK0F test\nValvano Oct 2017\nConnect analog signals to P9.0,P4.1,P9.1\n");
  EnableInterrupts();
  while(1){
    for(n=0; n<2000; n++){
      while(ADCflag == 0){};
      ADCflag = 0; // show every 2000th point
    }
    LCDOut2();
    UART0_OutUDec5(LeftConvert(nl));UART0_OutString(" mm,");
    UART0_OutUDec5(CenterConvert(nc));UART0_OutString(" mm,");
    UART0_OutUDec5(RightConvert(nr));UART0_OutString(" mm\n");
  }
}

int main(void){
//	Program15_1();
  Program15_2();
}
