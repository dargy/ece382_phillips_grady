/* Lab07_FSMmain.c
 * Runs on MSP432
 *
 * Student name: C2C Grady Phillips
 * Date: 17 Sept 20
 *
 * Redesigned by Capt Steven Beyer
 * Date: 9 September 2020
 * Student version of FSM lab, FSM with 2 inputs and 2 outputs.
 * Utilizes center IR sensors and motors.
 * Daniel and Jonathan Valvano
 * July 11, 2019
*/
/* This example accompanies the book
   "Embedded Systems: Introduction to Robotics,
   Jonathan W. Valvano, ISBN: 9781074544300, copyright (c) 2019
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2019, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

#include <stdint.h>
#include <stdio.h>
#include "msp.h"
#include "../inc/clock.h"
#include "../inc/CortexM.h"
#include "../inc/LaunchPad.h"
#include "../inc/Texas.h"
#include "../inc/Motor.h"
#include "../inc/Reflectance.h"
#include "../inc/SysTickInts.h"
#include "../inc/Bump.h"
#include "../inc/Nokia5110.h"

// Linked data structure
struct State {
  uint32_t out;                // 2-bit output
  uint32_t delay;              // time to delay in 1ms
  const struct State *next[4]; // Next if 2-bit input is 0-3
  char name[9];
};
typedef const struct State State_t;

// INSERT CODE
#define Center      &fsm[0]
#define Left1       &fsm[1]
#define Left2       &fsm[2]
#define WayLeft1    &fsm[3]
#define WayLeft2    &fsm[4]
#define LostLeft    &fsm[5]
#define Right1      &fsm[6]
#define Right2      &fsm[7]
#define WayRight1   &fsm[8]
#define WayRight2   &fsm[9]
#define LostRight   &fsm[10]
// student starter code

State_t fsm[11] = {
  {0x03, 100,   { WayRight1,    Left1,  Right1, Center  }, "Center   "}, // Center
  {0x02, 50,    { WayLeft1,     Left2,  Right1, Left2   }, "Left1    "}, // Left1
  {0x03, 50,    { Left1,        Left1,  Right2, Center  }, "Left2    "}, // Left2
  {0x02, 100,   { WayLeft2,     Left2,  Right1, Left2   }, "WayLeft1 "}, // WayLeft1
  {0x03, 50,    { LostLeft,     Left2,  Left2,  Left2   }, "WayLeft2 "}, // WayLeft2
  {0x00, 25,    { LostLeft,     Left2,  Left2,  Left2   }, "LostLeft "}, // LostLeft
  {0x01, 50,    { WayRight1,    Left1,  Right2, Right2  }, "Right1   "}, // Right1
  {0x03, 50,    { Right1,       Left2,  Right1, Center  }, "Right2   "}, // Right2
  {0x01, 100,   { WayRight2,    Left1,  Right2, Right2  }, "WayRight1"}, // WayRight1
  {0x03, 50,    { LostRight,    Right2, Right2, Right2  }, "WayRight2"}, // WayRight2
  {0x00, 25,    { LostRight,    Right2, Right2, Right2  }, "LostRight"}  // LostRight
};

// ***********testing of FSM*********
#define SPEED 2500;

State_t *Spt;  // pointer to the current state
uint8_t Input;
uint32_t Output;
uint16_t Left, Right;
uint16_t SpeedL, SpeedR;
uint8_t Sensor;
int32_t Position;

/********Helper functions******/
void CheckBump(void){
  int i;
  if(Bump_Read()==0){
    return; // no collision
  }
  Motor_Stop();       // stop
  uint8_t bump = Bump_Read();
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(200); LaunchPad_Output(0); // off
    Clock_Delay1ms(200); LaunchPad_Output(1); // red
  }
  while(Bump_Read()==0){// wait for touch
    Clock_Delay1ms(100);
    LaunchPad_Output(0); // off
    Clock_Delay1ms(100);
    LaunchPad_Output(3); // red/green
  }
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(100);
    LaunchPad_Output(0); // off
    Clock_Delay1ms(100);
    LaunchPad_Output(4); // blue
  }
  for(i=500;i>100;i=i-100){
    Clock_Delay1ms(i); LaunchPad_Output(0); // off
    Clock_Delay1ms(i); LaunchPad_Output(2); // green
  }
}

void LCDClear(void){
    Nokia5110_Init();
    Nokia5110_Clear();
    Nokia5110_OutString("Lab 7 FSM");
    Nokia5110_SetCursor(0,1); Nokia5110_OutString("Line follow");
    Nokia5110_SetCursor(0,2); Nokia5110_OutString("I="); Nokia5110_OutHex7(Input); Nokia5110_OutString(", O="); Nokia5110_OutHex7(Output);
    Nokia5110_SetCursor(0,4); Nokia5110_OutString("IR =");
    Nokia5110_SetCursor(0,5); Nokia5110_OutString("Pos=");
}

void LCDOut(void){
    Nokia5110_SetCursor(2,2); Nokia5110_OutHex7(Input); Nokia5110_OutString(", O="); Nokia5110_OutHex7(Output);
    Nokia5110_SetCursor(0,3); Nokia5110_OutString((char *)Spt->name);
    Nokia5110_SetCursor(4,4); Nokia5110_OutUHex7(Sensor);
    Nokia5110_SetCursor(4,5); Nokia5110_OutSDec(Position);
}
/**********MAIN************/
 /*Run FSM continuously
a) Output depends on State (Motors)
b) Wait depends on State
c) Input (Center reflectance sensors)
d) Next depends on (Input,State)
 */
int main(void){
	// Initialize all systems
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Motor_Init();
	Reflectance_Init();
    Bump_Init();
    LCDClear();
    Motor_Stop();
    EnableInterrupts();
    SpeedR = SpeedL = SPEED;
	// Initial state
    Spt = Center;
    while(1){
    	Output = Spt->out;

        if(Output&2) // if output bit 2 is 1
        {
            Left = SpeedL;
        }else{
            Left = 10;  // stopped
        }
        if(Output&1){ // if output bit 1 is 1
            Right = SpeedR;
        }else{
            Right = 10; // stopped
        }
        Motor_Forward(Left, Right);
        LCDOut();

        Clock_Delay1ms(Spt->delay);
        Sensor = Reflectance_Read(1000);

        // shift middle sensors
        Input = (Sensor >> 3) & 0x03;

        // Spt = next state indexed by input
        Spt = Spt->next[Input];

        Position = Reflectance_Position(Sensor);
        CheckBump();
    }
}
